/*
 * This is a stub response for testing purposes.  In a live app you'd get a similar
 * set of data from a backend via Ajax or it would be preloaded in the page.
 * The keys in this response match the properties of the Talk model (see model.talk.js)
 * so Backbone knows to bind them automatically when we create a new instance of that model in TalkListView's
 * 'initialize' call (see view.talklist.js)
*/

var JSON_DUMMY_DATA = [
	{
		title: 'Rapid Game Development in HTML5',
		summary: 'A look at how browser-based games can be quickly put together using Javascript and HTML5 technologies such as canvas, CSS3 and WebGL.',
		speaker: 'Neil',
		date: new Date(2013, 10, 20),
		tag: 'js'
	},
	{
		title: 'Introduction to AngularJS',
		summary: 'How using Angular can transform your web app development practices.',
		speaker: 'Chris G',
		date: new Date(2013, 9, 11),
		tag: 'js'
	},
	{
		title: 'CSS3 Animation Performance',
		summary: 'Tips for improving the speed and smoothness of your in-browser transitions.',
		speaker: 'Peter G',
		date: new Date(2013, 9, 04),
		tag: 'css'
	},
	{
		title: 'Nacelle and App Engine',
		summary: 'Introducing our new internal Python app framework.',
		speaker: 'Paddy',
		date: new Date(2013, 8, 15),
		tag: 'python'
	}
];
/**
 * A Model is a container for your data.  This is a simple model since there is no
 * business logic associated with it in our example.  In a production app this code
 * would also include methods to save the data to a storage layer (e.g. a database),
 * to pull data from third-party APIs (if this was a Tweet model, for example),
 * and logic to validate the data in the model e.g. character lengths, valid email,
 * missing fields, etc.
**/

var Talk = Backbone.Model.extend({

	/*
	should have a title
	should have a summary
	should have a speaker name
	should have an event date
	can have a tag
	*/

	// this is a list of all the model's properties.  The backend should return responses
	// in the same format so Backbone can automatically apply the data to the model instance.
	// These defaults are overwritten by the dummy data when the model is instantiated in
	// our TalkListView.initialize() method.

	defaults: function() {

		return {

			title: 'Untitled Talk',
			summary: 'This is a summary of the talk',
			speaker: 'Unknown',
			date: new Date(),
			tag: null

		};

	},

});



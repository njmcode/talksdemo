/**
 * A Router class in backbone to define the URLs the app can use as persistent states.
 * Should automatically handle URL history and suchlike
**/

var TalkRouter = Backbone.Router.extend({

		initialize: function() {

			// create a new instance of the TalkListView View.
			// Backbone Views automatically call their initialize() method when they are created
			this.app = new TalkListView();
			this.app.router = this;

		},

		// set up hash fragments that we will respond to
		// ':tagname' gets passed into the filterTag function as a variable
		routes: {
			'tag/:tagname': 'filterTag',
			'': 'index'
		},

		filterTag: function(tagname) {

			console.log('TalkRouter:filterTag', tagname);

			this.app.applyFilter(tagname); 	// this.app = TalkListView
			this.navigate('tag/' + tagname);	// put this URL fragment in the history

		},

		index: function() {

			// what to do if the fragment is empty

			console.log('TalkRouter:index');
			this.app.applyFilter('clear');
			this.navigate('');


		}

});



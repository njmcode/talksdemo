/**
 * A view for one item in the Talks list.
 * The app will have one of these for each list item currently in the view.
 * They are created during TalkListView's initialize() method.
**/

var TalkListItemView = Backbone.View.extend({

	/*
	should have a title
	should have a date
	should be tinted if it is in the future
	should have a toggle control
	the toggle control
		should expand the item if it is collapsed
		should collapse the item if it is expanded
	expanded items
		should show their summary and tag
	*/

	// cache the contents of the script tag template in index.html so we can use it in render()
	template: _.template($('#talk-item-template').html()),

	// event bindings as usual
	events: {

		'click .toggle' : 'toggleExpand'

	},

	// we don't define an explicit 'el' for this View since the element is not
	// in the DOM at runtime (it's in a script tag instead).  Here, when initialize()
	// is called, Backbone will create a new <div> with a class of '.item' and fill it
	// with our template output for this View.
	className: 'item',

	_expanded: false,	// just a private variable to track internally if this View should be expaned.

	initialize: function() {

		// nothing to put in here for now as there is no special logic needed
		// when we create a new list item.
		// If we were actively updating our models (e.g. allowing live edits of
		// their data on the frontend) we would put listeners in here
		// to update the view when the model changes

	},


	// Backbone gives each View an '$el' property, which is a jQuery-ready reference to
	// the View's 'el'.  Our 'el' is implicitly created since className is present, see above.
	// Here, we populate the 'el' for this View with our template, processed with our model instance data
	render: function() {

		// This View was given a Talk model instance in TalkListView.initialize()
		// this.model refers to that Talk instance, and has data from the dummy response in it

		var myData = this.model.toJSON();	// toJSON is a Backbone builtin to get our model in a format the templater can use
		var templateOutput = this.template(myData);	// replace our template placeholders with data from our model
		this.$el.html(templateOutput);	// set this View's el with our processed content
		this.$el.toggleClass('expanded', this._expanded);	// apply the 'expanded' class if we need to

		// compare the current date with the model's 'date' property and apply the '.future' class to our el if it's in the future
		// this allows us to apply a different CSS style to upcoming talks in the list
		var now = new Date();
		var then = this.model.get('date');
		if (now < then) this.$el.addClass('future');

		return this;

	},

	// toggle the expand state of this View and apply/remove the 'expanded' CSS class as needed
	toggleExpand: function(e) {

		console.log('TalkListItemView: toggle expand');

		e.preventDefault();

		this._expanded = !this._expanded;	// since _expanded is a boolean, you toggle it by NOT-ing it (i.e. true = !true, or !true = true)
		this.$el.toggleClass('expanded', this._expanded);

	},

	// explicitly collapse this View.  Called from TalkListView.collapseAll()
	collapse: function() {

		this._expanded = false;
		this.$el.removeClass('expanded');

	},

});

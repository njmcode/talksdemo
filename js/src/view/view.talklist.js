/**
 * A Backbone view to manage the drawing and filtering of the list of items.
 * Only one of these is present in the app; it is instantiated at the end of index.html
 *
 * Backbone.View.extend({ extra bits }) is how we inherit from the base View model and modify it to suit our needs
**/

var TalkListView = Backbone.View.extend({

	// Here I like to paste the requirements from the specification into the class
	// just so I can see easily what I need to implement in this View.
	// In production, we'd write unit tests against each of these requirements
	// to make sure they're properly implemented.

	/*
	should have a tag list
		filters the list by the selected tag on click
	should have a ‘clear filter’ control
		lists all items when clicked
	should have a Twitter-style ‘collapse all’ control
		closes all expanded items in the list on click
	should have a list of talks
	*/

	router: null,	// TalkRouter passes its context in here so we can call its functions from inside this view

	el: $('#talks-list'),	// explicitly tell the View which DOM element it has control of

	_listContainer: $('#list'),		// just a reference to the list container element

	items: [],	// empty array that initialize() will fill with TalkListItemView instances

	_filteredOutput: [],	// second array to keep track of what should be *currently* shown, used by onFilter()

	/*

	The 'events' property is a Backbone convention.  Events and selectors on the
	left, methods to call on the right.

	You can only bind to selectors inside the View's 'el'.  This is why
	this particular view uses '#talks-list' as its el, so we can access #filters
	and #collapse-all inside of it.

	*/
	events: {

		'click #filters li a' : 'onFilter',
		'click .item .tag' : 'onFilter',
		'click #collapse-all' : 'collapseAll'

	},

	// this method is called whenever an instance of this View is created.
	// It's the first thing called after the new() call in index.html
	initialize: function() {

		// normally we'd load data from an XHR call or from preloaded DB data here,
		// but we'll just use the dummy data file as an example

		var loadedData = JSON_DUMMY_DATA;

		// Use Underscore's 'each' method to iterate through each of the objects in
		// the loaded dummy data array.  'itemData' refers to each object in turn.
		// The 'this' at the end of this method makes sure that 'this' inside the function
		// still refers to our View, and not to the individual dummy object
		_.each(loadedData, function(itemData) {

				var itemModel = new Talk(itemData);		// create a new Talk model instance, using this dummy object as data (see model.talk.js)
				var itemView = new TalkListItemView({ model: itemModel });		// create a new item View, and associate our new model instance with it (see view.talk.js)
				this.items.push(itemView);	// add the new View into our 'items' array for future reference

		}, this);

		// our render() method uses this._filteredOutput when rendering the list, so
		// make sure it initially contains all the TalkItemViews we just created
		this._filteredOutput = this.items;

		console.log('TalkListView: got data from', loadedData);
		console.log('TalkListView: created item list', this._filteredOutput);

		// call this view's rendering method immediately
		this.render();

	},

	// This method is responsible for drawing out the list into the page.
	// It gets called by initialize() first, and by onFilter() whenever the contents of
	// the list need to be updated
	render: function() {

		this._listContainer.html('');	// empty the container

		// iterate our _filterOutput array of TalkListItemViews, get the output of their render()
		// method, and jQuery append() it to our list container
		_.each(this._filteredOutput, function(item) {

				console.log('TalkListView: rendering item', item);

				var itemOutput = item.render();
				this._listContainer.append(itemOutput.el);

				// the definitions in the 'events' object at the head of the TalkListItemView class get removed if the
				// view's DOM element is destroyed, as it was when we emptied the container at the start of
				// the render() method.  delegateEvents() is a Backbone in-built method which binds them again
				item.delegateEvents();

		}, this);

		console.log('TalkListView: list rendered');

		// nothing else happens now without user interaction, based on the 'events'
		// objects in this view and those in TalkListItemView.

	},

	// This method is bound in the 'events' object up top.
	// It is called when the user clicks on one of our filter links or tags.
	// 'e' is the mouse click event, passed in so we can catch it
	onFilter: function(e) {

		e.preventDefault(); // prevent the mouse click from doing anything else (e.g. scrolling the page or following the link)

		// get the 'data-type' attribute from the link we clicked on in order to know what tag we should filter by
		var filterType = $(e.currentTarget).data('type');

		this.applyFilter(filterType);

	},

	// actually applies a filter to the list.
	// Abstracted this out of onFilter so it can be called independently of DOM elements
	applyFilter: function(filterType) {

		console.log('TalkListView: filter applied', filterType);

		// empty the _filteredOutput array in preparation for populating it again
		this._filteredOutput = [];

		// iterate each of our initial items (loaded in initialize()), and decide if we should filter them or not
		_.each(this.items, function(item) {

				item.collapse();	// make sure the item is collapsed before we re-render

				// compare the clicked filter against the 'tag' property of the model associated with each view.
				// if it's a match, we want to render this item, so put it in the _filteredOutput array.
				// We also put it in if the user is clearing the filter (e.g. all items should be shown)
				if(filterType == 'clear' || filterType == item.model.get('tag')) this._filteredOutput.push(item);

		}, this);

		// update the history with the filter URL
		var route = (filterType == 'clear') ? '' : 'tag/' + filterType;
		this.router.navigate(route);

		// call render() again, which will use our updated _filteredOutput array
		this.render();

	},

	// just a simple event handler to collapse all the TalkListItemViews, like Twitter.com
	collapseAll: function(e) {

		// iterate our initial array of TalkListItemViews and call each one's collapse() method
		_.each(this.items, function(item) {

				item.collapse();

		});

	}


});
